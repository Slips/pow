# Pow

pow is a lightning fast AUR searcher written in C. It is compiled in c99, is fully POSIX.1-2008 compliant, and written in a suckless style. 

## Dependencies
curl

tcc (NOTE: gcc can technically be used, but gcc [is written in C++, therefore it is anti-suckless, and therefore sucks.](https://suckless.org/sucks/)

json-c (Generally provided by most package managers, chances are you already have this.)

scdoc

## Installation
`make install` will install the package to your system and add a manpage for it.

## Compilation
Compilation instructions are covered in the Makefile, but if you're lazy, here's the full compiler command:
```
tcc -std=c99 pow.c -o pow -O3 -Wall -Wpedantic -ljson-c
```

Alternatively, just run `make`.
