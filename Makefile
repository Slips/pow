##
# Zap: A lightning fast AUR Searcher written in C
#
# @file
# @version 0.1
.DEFAULT_GOAL := build
CFLAGS=-std=c99 -Wall -Wpedantic
OPT=-O3
LDFLAGS=-ljson-c
DESTDIR=/usr
CC=tcc

build:
	$(CC) pow.c -o pow $(OPT) $(CFLAGS) $(LDFLAGS)

man:
	scdoc < pow.1.scd > pow.1

install: build man
	cp -f pow $(DESTDIR)/bin/
	cp -f pow.1 $(DESTDIR)/share/man/man1/; true
# end
debug:
	gcc pow.c -o pow -g $(CFLAGS) $(LDFLAGS)
