/*
 * POW
 * A Lightning fast AUR searcher written in C
 * Contact:
 * Slips (slipfox underscore xyz at riseup dot net)
*/
/*System-provided libs*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
/*Third party libs*/
#include <json-c/json.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
/*local libs*/
#include "./colors/colors.h"

#define _POSIX_C_SOURCE 200809L

#define JSON_File(fname) json_object_from_file(fname)

#define JSON_Get(source, property) json_object_object_get((source), (property))

#define JSON_Put(objname) json_object_put(objname)

#define JSON_String(objname) json_object_get_string(objname)

#define JSON_Int(objname) json_object_get_int(objname)

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))


/*CONFIG*/
#define argv_parse_len 100 /*DO NOT TOUCH UNLESS YOU KNOW WHAT YOU ARE DOING.*/

#define hardMaxResults 50 /*Maximum amount of results pow will show. Set to 0 to disable (not recommended)*/

/*CODE*/

/*typedef our json_object*/
typedef json_object JSON;
/*defining a struct here to lower the amount of json_objects pow allocates.*/
struct aurItem {
    const char *maintainer;
    time_t      firstsub;
    time_t      outOfDate;
    time_t      lastModified;
    const char *upStream;
    const char *name;
    const char *desc;
    const char *version;
};
    struct tm ts;
/*predeclarations for our functions*/
static int aurInfo(char *input);
static int aurSearch(char *input);
static int aurLink(char *input);
static int aurClone(char *input);
static void helpMsg();
/*necessary for error handling*/
extern int errno;
int
aurInfo(char *input){

        json_object *arrayItem;
        pid_t finished;
        pid_t status;
        int hardMaxRes;
        char buf[20];

        char baseURL[argv_parse_len+52] = {"https://aur.archlinux.org/rpc/?v=5&type=info&arg="};
        char *fullURL[] = {"curl", "-s", strncat(baseURL, input, argv_parse_len), "-o", "/tmp/pow-pkginfo.json", (char *)0};
        pid_t forkPID = fork();
        switch (forkPID) {
        case -1: /*pow is infertile for whatever reason*/
            return 1;
            break;;
        case 0: /*pow has successfully conceived*/
            errno = 0;
            execvp("curl", fullURL);
            printf("%i: %s\n",errno, strerror(errno));
            return -errno;
            break;;
        default:
            finished = wait(&status);

            if(finished != forkPID){
                fputs(C_RED "FATAL: got PID of unknown process (Expected PID of Child)\n" C_RESET, stderr);
                return 1;
            }
            break;;
            }

        json_object *root = JSON_File("/tmp/pow-pkginfo.json");
        json_object *results  = JSON_Get(root, "results");

        /*If json_object is invalid, print as such and return 1.*/
        if (!root){
            fputs(C_RED "FATAL: json file invalid.\n" C_RESET, stderr);
            return 1;
        }
        json_object *resultCount = JSON_Get(root, "resultcount");
        /*declare resultCount as int just for zero-match checking. Not used elsewhere.*/
        int resultCountVar = atoi(JSON_String(resultCount));
        if (resultCountVar == 0){printf("0 results for search " C_BOLD "%s.\n\n" C_RESET, input);}
        int jsonMaxResults = json_object_array_length(results);
        if(hardMaxResults == 0) {
            hardMaxRes = jsonMaxResults;
        } else {
            hardMaxRes = hardMaxResults;
        }
        /*If hardMaxResults is defined to zero, set no max*/
        for (int i = 0; i < MIN(jsonMaxResults,hardMaxRes); i++)
            {
            /*get each full item in the array.*/
            arrayItem = json_object_array_get_idx(results, i);
            /*Declare items in our struct for each thing we'll want*/
            struct aurItem aurItem;
            aurItem.desc = JSON_String(JSON_Get(arrayItem, "Description"));
            aurItem.maintainer = JSON_String(JSON_Get(arrayItem, "Maintainer"));
            aurItem.name = JSON_String(JSON_Get(arrayItem, "Name"));
            aurItem.version = JSON_String(JSON_Get(arrayItem, "Version"));
            aurItem.upStream = JSON_String(JSON_Get(arrayItem, "URL"));
            aurItem.outOfDate = JSON_Int(JSON_Get(arrayItem, "OutOfDate"));
            aurItem.lastModified = JSON_Int(JSON_Get(arrayItem, "LastModified"));
            aurItem.firstsub = JSON_Int(JSON_Get(arrayItem, "FirstSubmitted"));

            /*Declaring these 3 as json_object's because they're arrays and that scares me */
            json_object *depends = JSON_Get(arrayItem, "Depends");
            json_object *makedepends = JSON_Get(arrayItem, "MakeDepends");
            json_object *optdepends = JSON_Get(arrayItem, "OptDepends");
            /*Why the hell is this even an array? How often are people actually setting multiple licenses? argh*/
            json_object *license = JSON_Get(arrayItem, "License");
            printf(C_BOLD "Name: " C_RESET "%s\n", aurItem.name);
            if(aurItem.desc) {
                printf(C_BOLD "Description: " C_RESET "%s\n", aurItem.desc);
            } else {
                printf(C_BOLD "Description: " C_RESET C_RED "No Description Available\n\n");
            }
            if(aurItem.maintainer) {
            printf(C_BOLD "Maintainer: " C_RESET "%s\n", aurItem.maintainer);
            } else {
                printf(C_BOLD "Maintainer: " C_RESET C_RED "Package Orphaned\n" C_RESET);
            }
            printf(C_BOLD "Version: " C_RESET "%s\n", aurItem.version);
            fputs(C_BOLD "License: " C_RESET, stdout);
            /*Iterates through array, prints each dependency in a list*/
            for(unsigned long i = 0; i < json_object_array_length(license); i++) {
                if(i != json_object_array_length(license) - 1) {
                    const char *str = json_object_get_string(json_object_array_get_idx(license, i));
                    printf("%s, ", str);
                } else {
                    const char *str = json_object_get_string(json_object_array_get_idx(license, i));
                    printf("%s\n\n", str);
                }
                }
            fputs(C_BOLD "Dependencies: " C_RESET, stdout);
            if(depends) {
                for(unsigned long i = 0; i < json_object_array_length(depends); i++) {
                    if(i != json_object_array_length(depends) - 1) {
                        const char *str = json_object_get_string(json_object_array_get_idx(depends, i));
                        printf("%s, ", str);
                    } else {
                        const char *str = json_object_get_string(json_object_array_get_idx(depends, i));
                        printf("%s\n", str);
                    }
                }
            } else {
                fputs("None\n",stdout);
            }
            if(JSON_String(optdepends)) {
                fputs(C_BOLD "Optional Dependencies: " C_RESET, stdout);
                for(unsigned long i = 0; i < json_object_array_length(optdepends); i++) {
                    if(i != json_object_array_length(optdepends) - 1) {
                        const char *str = json_object_get_string(json_object_array_get_idx(optdepends, i));
                        printf("%s, ", str);
                    } else {
                        const char *str = json_object_get_string(json_object_array_get_idx(optdepends, i));
                        printf("%s\n", str);
                    }
                }
            }
            if(JSON_String(makedepends)) {
            fputs(C_BOLD "Make Dependencies: " C_RESET, stdout);
            for(unsigned long i = 0; i < json_object_array_length(makedepends); i++) {
                if(i != json_object_array_length(makedepends) - 1) {
                    const char *str = json_object_get_string(json_object_array_get_idx(makedepends, i));
                    printf("%s, ", str);
                } else {
                    const char *str = json_object_get_string(json_object_array_get_idx(makedepends, i));
                    printf("%s\n\n", str);
                }
            }
            }
            if(aurItem.outOfDate) {
                ts = *localtime(&aurItem.outOfDate);
                strftime(buf, sizeof(buf), "%m/%d/%Y", &ts);
                printf(C_BOLD "Out Of Date: " C_RESET C_RED "Yes (%s)\n" C_RESET, buf);
            } else {
                fputs(C_BOLD "Out of Date: " C_RESET "No\n", stdout);
            }
            ts = *localtime(&aurItem.firstsub);
            strftime(buf, sizeof(buf), "%m/%d/%Y %H:%M:%S", &ts);
            printf(C_BOLD "First Submitted: " C_RESET "%s\n", buf);
            ts = *localtime(&aurItem.lastModified);
            strftime(buf, sizeof(buf), "%m/%d/%Y %H:%M:%S", &ts);
            printf(C_BOLD "Last Modified: " C_RESET "%s\n\n", buf);

            printf(C_BOLD "Upstream URL: " C_RESET "%s\n", aurItem.upStream);
            printf(C_BOLD "Clone URL: " C_RESET "https://aur.archlinux.org/%s.git\n", aurItem.name);
            printf(C_BOLD "Info URL: " C_RESET "https://aur.archlinux.org/packages/%s\n\n", aurItem.name);
            }
    /*Cleaning up JSON objects (getting rid of root gets rid of results as well.)*/
    JSON_Put(root);
    remove("/tmp/pow-search.json");
    return 0;
}
int
aurSearch(char *input){

        json_object *arrayItem;
        pid_t finished;
        pid_t status;
        int hardMaxRes;
        char buf[11];

        char baseURL[argv_parse_len+65] = {"https://aur.archlinux.org/rpc/?v=5&type=search&by=name-desc&arg="};
        char *fullURL[] = {"curl", "-s", strncat(baseURL, input, argv_parse_len), "-o", "/tmp/pow-search.json", (char *)0};
        pid_t forkPID = fork();
        switch (forkPID) {
        case -1: /*pow is infertile for whatever reason*/
            return 1;
            break;;
        case 0: /*pow has successfully conceived*/
            errno = 0;
            execvp("curl", fullURL);
            printf("%i: %s\n",errno, strerror(errno));
            return -errno;
            break;;
        default:
            finished = wait(&status);

            if(finished != forkPID){
                fputs(C_RED "FATAL: got PID of unknown process (Expected PID of Child)\n" C_RESET, stderr);
                return 1;
            }
            break;;
            }

        json_object *root = JSON_File("/tmp/pow-search.json");
        json_object *results  = JSON_Get(root, "results");

        /*If json_object is invalid, print as such and return 1.*/
        if (!root){
            fputs(C_RED "FATAL: json file invalid.\n" C_RESET, stderr);
            return 1;
        }
        json_object *resultCount = JSON_Get(root, "resultcount");
        /*declare resultCount as int just for zero-match checking. Not used elsewhere.*/
        int resultCountVar = atoi(JSON_String(resultCount));
        if (resultCountVar == 0){printf("0 results for search" C_BOLD "%s.\n\n" C_RESET, input);}
        int jsonMaxResults = json_object_array_length(results);
        if(hardMaxResults == 0) {
            hardMaxRes = jsonMaxResults;
        } else {
            hardMaxRes = hardMaxResults;
        }
        /*If hardMaxResults is defined to zero, set no max*/
        for (int i = 0; i < MIN(jsonMaxResults,hardMaxRes); i++)
            {
            /*get each full item in the array.*/
            arrayItem = json_object_array_get_idx(results, i);
            /*Declare items in our struct for each thing we'll want*/
            struct aurItem aurItem;
            aurItem.desc = JSON_String(JSON_Get(arrayItem, "Description"));
            aurItem.maintainer = JSON_String(JSON_Get(arrayItem, "Maintainer"));
            aurItem.name = JSON_String(JSON_Get(arrayItem, "Name"));
            aurItem.outOfDate = JSON_Int(JSON_Get(arrayItem, "OutOfDate"));

            /*If maintainer is null, print [Orphaned] in it's place*/
            if(!aurItem.maintainer) { printf(C_RED "[Orphaned]" C_RESET C_BLUE C_BOLD "/" C_RESET C_DIM "%s" C_RESET, aurItem.name); } else
            {
            printf(C_DIM "%s" C_RESET C_BLUE C_BOLD "/" C_RESET C_DIM "%s" C_RESET, aurItem.maintainer, aurItem.name);
            }
            /*If outOfDate returns anything (i.e doesn't return null), list as out of date*/
            if(aurItem.outOfDate) {
                fputs(C_RED " [Out Of Date |" C_RESET , stdout);
                ts = *localtime(&aurItem.outOfDate);
                strftime(buf, sizeof(buf), "%m/%d/%Y", &ts);
                printf(C_RED " %s]" C_RESET, buf);
            }
            /*If description is null, print (No Description) in it's place*/
            if(!aurItem.desc) { printf("\n(No Description)\nhttps://aur.archlinux.org/%s.git\n\n", aurItem.name);} else
                {
            printf(C_BOLD C_ITALIC "\n%s\n" C_RESET C_BOLD "https://aur.archlinux.org/%s.git\n\n", aurItem.desc, aurItem.name);
            }

    }
    /*Cleaning up JSON objects (getting rid of root gets rid of results as well.)*/
    JSON_Put(root);
    remove("/tmp/pow-search.json");
    return 0;
}

int
aurClone(char *input){
    pid_t finished;
    pid_t status;
    /*
     * Check for a clipboard utility. which returns 0 if successful and returns 1 if it cannot find the program.
     */
    json_object *arrayItem;
    char baseURL[argv_parse_len+52] = {"https://aur.archlinux.org/rpc/?v=5&type=info&arg="};
    char *fullURL[] = {"curl", "-s", strncat(baseURL, input, argv_parse_len), "-o", "/tmp/pow-link.json", (char *)0};
    pid_t forkPID = fork();
    switch (forkPID) {
    case -1: /*pow is infertile for whatever reason*/
        return 1;
        break;;
    case 0: /*pow has successfully conceived*/
        errno = 0;
        execvp("curl", fullURL);
        printf("%i: %s\n",errno, strerror(errno));
        return -errno;
        break;;
    default:
        finished = wait(&status);

        if(finished != forkPID){
            fputs(C_RED "FATAL: got PID of unknown process (Expected PID of Child)\n" C_RESET, stderr);
            return 1;
        }
        break;;
        }
    json_object *root = JSON_File("/tmp/pow-link.json");
    json_object *results  = JSON_Get(root, "results");


    if (!root) {
        fputs(C_RED "FATAL: JSON file invalid.\n" C_RESET, stderr);
        remove("/tmp/pow-link.json");
        return 1;
    }

    json_object *resultCount = JSON_Get(root, "resultcount");
    int resultCountVar = atoi(JSON_String(resultCount));

    if (resultCountVar == 0) {
        printf("Package " C_BOLD "\"%s\"" C_RESET " not found.\n\n", input);
    }

    int jsonMaxResults = json_object_array_length(results);

    for (int i = 0; i < jsonMaxResults; i++) {

        arrayItem = json_object_array_get_idx(results, i);
        /*Declare items in our struct for each thing we'll want*/
        struct aurItem aurItem;
        aurItem.name = JSON_String(JSON_Get(arrayItem, "Name"));

        char aURL[argv_parse_len+35] = {"git clone -q https://aur.archlinux.org/"};
        strcat(aURL, aurItem.name);
        strcat(aURL, ".git");
        system(aURL);
        printf(C_BOLD "https://aur.archlinux.org/%s.git" C_RESET " cloned in current directory.\n\n", aurItem.name);

    }
    /*GTFO of here json_objects :p*/
    JSON_Put(root);
    remove("/tmp/pow-clone.json");
    return 0;


}
int
aurLink(char *input){
    pid_t finished;
    pid_t status;
    /*
     * Check for a clipboard utility. which returns 0 if successful and returns 1 if it cannot find the program.
     */
    char *cliptool = ""; /*Declared here as empty just to fix a maybe-uninitalized warning later on.*/
    json_object *arrayItem;
    if(system("which xclip > /dev/null")){
        if(!system("which xsel > /dev/null")) {
            cliptool = "xsel -bi";
        } else {
            fputs(C_RED "FATAL: X clipboard utility not detected. Please install either xclip or xsel." C_RESET, stderr);
        }
    } else {
        cliptool = "xclip -selection clipboard";
    }
    char baseURL[argv_parse_len+52] = {"https://aur.archlinux.org/rpc/?v=5&type=info&arg="};
    char *fullURL[] = {"curl", "-s", strncat(baseURL, input, argv_parse_len), "-o", "/tmp/pow-link.json", (char *)0};
    pid_t forkPID = fork();
    switch (forkPID) {
    case -1: /*pow is infertile for whatever reason*/
        return 1;
        break;;
    case 0: /*pow has successfully conceived*/
        errno = 0;
        execvp("curl", fullURL);
        printf("%i: %s\n",errno, strerror(errno));
        return -errno;
        break;;
    default:
        finished = wait(&status);

        if(finished != forkPID){
            fputs(C_RED "FATAL: got PID of unknown process (Expected PID of Child)\n" C_RESET, stderr);
            return 1;
        }
        break;;
        }
    json_object *root = JSON_File("/tmp/pow-link.json");
    json_object *results  = JSON_Get(root, "results");


    if (!root) {
        fputs(C_RED "FATAL: JSON file invalid.\n" C_RESET, stderr);
        remove("/tmp/pow-link.json");
        return 1;
    }

    json_object *resultCount = JSON_Get(root, "resultcount");
    int resultCountVar = atoi(JSON_String(resultCount));

    if (resultCountVar == 0) {
        printf("Package " C_BOLD "\"%s\"" C_RESET " not found.\n\n", input);
    }

    int jsonMaxResults = json_object_array_length(results);

    for (int i = 0; i < jsonMaxResults; i++) {

        arrayItem = json_object_array_get_idx(results, i);
        /*Declare items in our struct for each thing we'll want*/
        struct aurItem aurItem;
        aurItem.name = JSON_String(JSON_Get(arrayItem, "Name"));

        char aURL[argv_parse_len+35] = {"echo https://aur.archlinux.org/"};
        strcat(aURL, aurItem.name);
        strcat(aURL, ".git | ");
        strcat(aURL, cliptool);
        system(aURL);
        printf(C_BOLD "https://aur.archlinux.org/%s.git" C_RESET " copied to clipboard.\n\n", aurItem.name);

    }
    /*GTFO of here json_objects :p*/
    JSON_Put(root);
    remove("/tmp/pow-link.json");
    return 0;


}
void
helpMsg(){
        fputs("Pow: A lightning fast AUR Searcher written in C.\n", stdout);
        fputs("------------------------------------------------\n", stdout);
        fputs("s KEYWORD               | Search for a term.\n", stdout);
        fputs("                        |\n", stdout);
        fputs("l PACKAGENAME           | Copy the link to a package\n", stdout);
        fputs("                        | to your clipboard. (requires xclip or xsel.)\n", stdout);
        fputs("                        |\n", stdout);
        fputs("i PACKAGENAME           | Show in-depth package info.\n", stdout);
        fputs("                        |\n", stdout);
        fputs("c PACKAGENAME           | Clone a package to pwd.\n", stdout);
        fputs("                        |\n", stdout);
        fputs("h                       | Help\n", stdout);
        fputs("------------------------------------------------\n", stdout);
}

int
main(int argc, char *argv[]) {
    char my_str[argv_parse_len] = {0};

    /*Beginning arg checks*/
    if(argc == 1) {
        helpMsg();
        return 1;
    }
    switch (argv[1][0]) {
    case 's': /* Search*/
        if(argc >= 3)
            {
        for (int i = 2; i < argc; i++) {
            /* Concatenate the argument with strncat into a string*/
            strncat(my_str, argv[i], argv_parse_len-1); /* -1 to keep a NUL byte just in case*/
            strncat(my_str, "_", argv_parse_len-1); /* -1 to keep a NUL byte just in case*/
        }
        my_str[strlen(my_str)-1] = '\0';
        printf(C_ITALIC "Searching for %s...\n\n" C_RESET, my_str);
        aurSearch(my_str);
        } else {
            fputs(C_RED "FATAL: No search keyword provided.\n" C_RESET, stderr);
        }
        break;
    case 'l':
        if(argc >= 3)
            {
        for (int i = 2; i < argc; i++) {
            /* Concatenate the argument with strncat into a string*/
            strncat(my_str, argv[i], argv_parse_len-1); /* -1 to keep a NUL byte just in case*/
            strncat(my_str, "_", argv_parse_len-1); /* -1 to keep a NUL byte just in case*/
        }
        my_str[strlen(my_str)-1] = '\0';
        printf(C_ITALIC "Fetching link for %s...\n\n" C_RESET, my_str);
        aurLink(my_str);
        } else
            {
            fputs(C_RED "FATAL: No search keyword provided.\n" C_RESET, stderr);
        }
        break;
    case 'i':
        if(argc >= 3)
            {
        for (int i = 2; i < argc; i++) {
            /* Concatenate the argument with strncat into a string*/
            strncat(my_str, argv[i], argv_parse_len-1); /* -1 to keep a NUL byte just in case*/
            strncat(my_str, "_", argv_parse_len-1); /* -1 to keep a NUL byte just in case*/
        }
        my_str[strlen(my_str)-1] = '\0';
        printf(C_ITALIC "Fetching info for %s...\n\n" C_RESET, my_str);
        aurInfo(my_str);
        } else
            {
            fputs(C_RED "FATAL: No search keyword provided.\n" C_RESET, stderr);
        }
        break;

    case 'c':
        if(argc >= 3)
            {
        for (int i = 2; i < argc; i++) {
            /* Concatenate the argument with strncat into a string*/
            strncat(my_str, argv[i], argv_parse_len-1); /* -1 to keep a NUL byte just in case*/
            strncat(my_str, "_", argv_parse_len-1); /* -1 to keep a NUL byte just in case*/
        }
        my_str[strlen(my_str)-1] = '\0';
        printf(C_ITALIC "Cloning %s...\n\n" C_RESET, my_str);
        aurClone(my_str);
        } else
            {
            fputs(C_RED "FATAL: No search keyword provided.\n" C_RESET, stderr);
        }
        break;
    case 'h': /* Help*/
        helpMsg();
        break;
    default:
        helpMsg();
        break;
    }
}
/*
 * Credits:
 * Slips - Me, developed the program.
 * Bowuigi - Inspiration, wrote the lua-based zap AUR searcher (which this is a spiritual successor to.)
 * also provided the colors library this software uses and helped me along the way with many things writing this software.
 * You - Selling me your soul by reading the EULA you retroactively signed by reading this source code ;)
 * (In case I need to state as such, that is a joke. This software has no EULA.)
 *
*/
